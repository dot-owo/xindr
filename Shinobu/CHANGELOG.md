# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Hex-hm-v3.0.0-patch1] - 2024-06-18

### Updated

* Upgraded to version 24.05


## [Hex-nhm-v1.0.0-patch2] - 2024-02-29

* Generation 26

### Added

- NixOS Label

### Updated

- VSCodium Configuration


## [Hex-nhm-v1.0.0-patch1] - 2024-02-29

* Generation 25

### Fixed

- Deprecated Unstable link for nixpkgs


## [Hex-nhm-v1.0.0] - 2024-02-29

* Generation 24

### Added

#### Packages
- VS Codium
