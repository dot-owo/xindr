# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, options, ... }:

let
  unstableTarball = 
    fetchTarball
      https://github.com/NixOS/nixpkgs/archive/nixos-unstable.tar.gz;
in
{
  system.nixos.label = "Hex-hm-v3.0.0-patch1";
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  nix = {
    settings = {
      experimental-features = [ "nix-command" "flakes" ];
    };
  };

  nixpkgs.config = {
    packageOverrides = pkgs: with pkgs; {
      unstable = import unstableTarball {
        config = config.nixpkgs.config;
      };
    };
    allowUnfree = true;
  };

  # Use the systemd-boot EFI boot loader.
	boot.loader = {
	  efi = {
	    canTouchEfiVariables = true;
	    efiSysMountPoint = "/boot/efi"; # ← use the same mount point here.
	  };
	  grub = {
	     efiSupport = true;
	     #efiInstallAsRemovable = true; # in case canTouchEfiVariables doesn't work for your system
	     device = "nodev";
	     useOSProber = true;
	     configurationLimit = 17;
	     extraConfig = ''
	         set timeout=-1
	     '';
	  };
	};

  networking.hostName = "Shinobu"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # AppImage
  boot.binfmt.registrations.appimage = {
    wrapInterpreterInShell = false;
    interpreter = "${pkgs.appimage-run}/bin/appimage-run";
    recognitionType = "magic";
    offset = 0;
    mask = ''\xff\xff\xff\xff\x00\x00\x00\x00\xff\xff\xff''; 
    magicOrExtension = ''\x7fELF....AI\x02'';
  };

  # Set your time zone.
  time.timeZone = "Asia/Manila";
  time.hardwareClockInLocalTime = false;
  
  # Fix time
  networking.timeServers = options.networking.timeServers.default ++ [ "time.cloudflare.com" ];
  services.chrony.enable = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkb.options in tty.
  # };

  # Enable the X11 windowing system.
  services.xserver.enable = true;


  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
  

  # Configure keymap in X11
  services.xserver.xkb.layout = "us";
  # services.xserver.xkb.options = "eurosign:e,caps:escape";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.esper = {
    isNormalUser = true;
    extraGroups = [ "wheel" "sudo" "docker" ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [
      zoxide
      syncthing
      kubectl
      warp-terminal
      go
     #unstable.zoxide
     #unstable.syncthing
     #unstable.kubectl
     #unstable.warp-terminal
     #unstable.go
      tlrc
      discord
      vencord
      spotify
      (vscode-with-extensions.override {
        vscode = unstable.vscodium;
        vscodeExtensions = with vscode-extensions; [
          jnoortheen.nix-ide
          arrterian.nix-env-selector
          vscodevim.vim
          eamodio.gitlens
	  golang.go
          catppuccin.catppuccin-vsc
          catppuccin.catppuccin-vsc-icons
	  # catppuccin.catppuccin-vsc-pack
        ];
      })
    ];
  };

# users.users.nolle = {
#   isNormalUser = true;
#   extraGroups = [ "wheel" "sudo" ]; # Enable ‘sudo’ for the user.
#   packages = with pkgs; [
#     unstable.zoxide
#     unstable.syncthing
#     tlrc
#     discord
#     vencord
#     spotify
#     (vscode-with-extensions.override {
#       vscode = unstable.vscodium;
#       vscodeExtensions = with vscode-extensions; [
#         jnoortheen.nix-ide
#         arrterian.nix-env-selector
#         vscodevim.vim
#         eamodio.gitlens
#         # Catppuccin.catppuccin-vsc
#         # Catppuccin.catppuccin-vsc-icons
#       ];
#     })
#   ];
# };



  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [

    home-manager

    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    neovim
    wget
    curl
    tmux
    tree
    gnupg
    zip
    unzip
    xarchiver
    git
    docker
    k3s
    k3d

    floorp
    chromium

    kitty
    neofetch

    desktop-file-utils
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
      PermitRootLogin = "no";
      #UsePAMAuthentication = true;
      Port = 12007;
    };
  };

  # Enable Docker
  virtualisation.docker = {
      enable = true;
     #rootless = {
     #    enable = true;
     #    setSocketVariable = true;
     #};
  };

  # Enable k3s
  services.k3s = {
    enable = false;
  };
  systemd.enableUnifiedCgroupHierarchy = false;

  # Open ports in the firewall.
  #networking.firewall.allowedTCPPorts = [ 6443 8080 8384 12007 22000 ];
  #networking.firewall.allowedUDPPorts = [ 6443 8080 22000 21027 ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}

